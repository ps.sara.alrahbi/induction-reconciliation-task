package progressoft.jip9.reader;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ListsComparison {
    private List<FileData> sourceList;
    private List<FileData> targetList;
    final SavedOutput savedOutput = new SavedOutput();
    HashMap<String, Data> potentialSMissingList = new HashMap<>();
    HashMap<String, Data> potentialTMissingList = new HashMap<>();

    public ListsComparison(FileInfo source, FileInfo target) throws IllegalFileInfoException {
        setLists(source.getFileDataList(), target.getFileDataList());
        sort();
    }

    public SavedOutput compareTheLists() {
        int size = checkSize();
        for (int i = 0; i < size; i++) {
            FileData sObj = sourceList.get(i);
            FileData tObj = targetList.get(i);
            String sId = sObj.getTransactionId();
            String tId = tObj.getTransactionId();
            if (isSameString(sId, tId)) {
                checkMatchedData(sObj, tObj);
            } else {
                potentialSMissingList.put(sId, new Data(sObj, TakenFile.SOURCE));
                potentialTMissingList.put(tId, new Data(tObj, TakenFile.TARGET));
            }
        }
        if (!potentialSMissingList.isEmpty()) {
            List<String> keys = new ArrayList<>();
            potentialSMissingList.forEach((k, v) -> {
                if (potentialTMissingList.containsKey(k)) {
                    checkMatchedData(v.getFileData(), potentialTMissingList.get(k).getFileData());
                    keys.add(k);
                }

            });
            for (String key : keys) {
                removeFromHash(key);
            }
            if(sourceList.size()!=targetList.size())
                checkWhenNotSameSize();

            if (!potentialSMissingList.isEmpty())
                savedOutput.addToMissing(potentialSMissingList);
            if (!potentialTMissingList.isEmpty())
                savedOutput.addToMissing(potentialTMissingList);

        }

        return savedOutput;
    }

    private void checkWhenNotSameSize() {
        if (targetList.size() > sourceList.size()) {
            putRestToMissing(targetList, sourceList, potentialTMissingList);
        }else{
            putRestToMissing(sourceList, targetList, potentialSMissingList);
        }
    }

    private void putRestToMissing(List<FileData> sourceList, List<FileData> targetList, HashMap<String, Data> potentialSMissingList) {
        int num = sourceList.size() - targetList.size();
        int limit = targetList.size()-1;
        for (int i = (limit + num); i >= limit; i--) {
            potentialSMissingList.put(sourceList.get(i).getTransactionId(), new Data(sourceList.get(i), TakenFile.TARGET));
        }
    }

    private int checkSize() {
        int size;
        if (targetList.size() > sourceList.size())
            size = sourceList.size();
        else
            size = targetList.size();
        return size;
    }


    private boolean isSameString(String transactionId, String transactionId2) {
        return transactionId.equalsIgnoreCase(transactionId2);
    }

    private void checkMatchedData(FileData sObj, FileData tObj) {
        if (checkFileData(sObj, tObj))
            savedOutput.addToMatched(sObj);
        else
            savedOutput.addToMismatched(new Data(sObj, TakenFile.SOURCE), new Data(tObj, TakenFile.TARGET));
    }

    private boolean checkFileData(FileData sData, FileData tData) {

        if (!(sData.getCurrencyCode().equals(tData.getCurrencyCode())))
            return false;
        else if (sData.getAmount().compareTo(tData.getAmount()) != 0)
            return false;
        return isSameString(sData.getValueDate(), tData.getValueDate());

    }


    protected void sort() {
        Collections.sort(sourceList);
        Collections.sort(targetList);
    }

    private void setLists(List<FileData> sourceList, List<FileData> targetList) throws IllegalFileInfoException {
        if (sourceList == null || targetList == null)
            throw new IllegalFileInfoException("null List");
        if (sourceList.isEmpty() || targetList.isEmpty())
            throw new IllegalFileInfoException("empty list");
        this.sourceList = sourceList;
        this.targetList = targetList;
    }


    public List<FileData> getTargetList() {
        return this.targetList;
    }


    private void removeFromHash(String k) {
        potentialTMissingList.remove(k);
        potentialSMissingList.remove(k);
    }
}
