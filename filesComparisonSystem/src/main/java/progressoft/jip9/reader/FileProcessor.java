package progressoft.jip9.reader;

import java.nio.file.Path;
import java.nio.file.Paths;


public class FileProcessor {


    public Path processFiles(FileInfo source, FileInfo target) throws IllegalFileInfoException {
        checkFiles(source, target);
        String sourceFormat = source.getFileFormat();
        StrategyReader reader = new StrategyReader(getReaderClass(sourceFormat));

        FileInfo sourceData = reader.readStrategy(source);

        reader.changeStrategy(getReaderClass(target.getFileFormat()));
        FileInfo targetData = reader.readStrategy(target);

        ListsComparison compare = new ListsComparison(sourceData, targetData);
        SavedOutput savedOutput = compare.compareTheLists();
        ListToFileConvertor convertor= new ListToFileConvertor();
        Path path = convertor.convertOutputToFile(savedOutput);


        return path.toAbsolutePath();
    }


    protected Reader getReaderClass(String format) {
        if (format.toLowerCase().contains("csv"))
            return new CSVReader();
        if (format.toLowerCase().contains("json"))
            return new JsonReader();
        return null;

    }

    private void checkFiles(FileInfo source, FileInfo target) throws IllegalFileInfoException {
        FileCheck fileCheck = new FileCheck();
        fileCheck.checkFile(source);
        fileCheck.checkFile(target);
    }


}
