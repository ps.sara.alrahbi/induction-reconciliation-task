package progressoft.jip9.reader;

import java.io.File;
import java.util.List;

public interface Reader {
    List<FileData> read(File file) throws IllegalFileInfoException;
}
