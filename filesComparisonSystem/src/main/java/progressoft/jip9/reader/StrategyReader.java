package progressoft.jip9.reader;

import java.util.List;

public class StrategyReader {
    private Reader reader;
    TakenFile takenFile;

    public StrategyReader(Reader reader) throws IllegalFileInfoException {
        if (reader==null)
            throw new IllegalFileInfoException("null reader");
        setStrategy(reader);
        takenFile = TakenFile.SOURCE;
    }

    public void changeStrategy(Reader reader) {
        setStrategy(reader);

    }

    private void setStrategy(Reader reader) {
        this.reader = reader;
    }

    public FileInfo readStrategy(FileInfo file) throws IllegalFileInfoException {
        List<FileData> list = reader.read(file.getFile());
        file.setFileDataList(list);
        return file;
    }

    //for testing purposes
    protected Reader getReader() {
        return reader;
    }
}
