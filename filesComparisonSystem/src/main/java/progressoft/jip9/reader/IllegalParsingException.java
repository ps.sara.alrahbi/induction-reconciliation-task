package progressoft.jip9.reader;

public class IllegalParsingException extends IllegalArgumentException {
    public IllegalParsingException(String msg) {
        super(msg);
    }

    public IllegalParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
