package progressoft.jip9.reader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileCheck {
    public void checkFile(FileInfo fileInfo) throws IllegalFileInfoException {
        checkForNullValues(fileInfo);
        checkIfItHasFilePath(fileInfo.getFile());
        checkFileType(fileInfo);

    }

    private void checkIfItHasFilePath(File file) throws IllegalFileInfoException {
        if (!file.exists() || !file.isFile())
            throwException("File path should be given");
    }

    private void checkFileType(FileInfo fileInfo) throws IllegalFileInfoException {
        String fileType = getFileType(fileInfo);
        String fileFormat = fileInfo.getFileFormat();
        if (!fileType.contains(fileFormat)) {
            if(!fileType.contains("vnd")&& fileFormat.equalsIgnoreCase("csv"))
            throwException("provided different format from file type: " +
                    "file type: " + fileType +
                    " format given: " + fileFormat);
        }
    }

    private String getFileType(FileInfo fileInfo) throws IllegalFileInfoException {
        String fileType = "";
        try {
            fileType = Files.probeContentType(fileInfo.getFile().toPath());
        } catch (IOException e) {
            throw new IllegalFileInfoException("can't define file type",e);
        }
        return fileType;
    }

    private void checkForNullValues(FileInfo fileInfo) throws IllegalFileInfoException {
        if (fileInfo == null)
            throwException("Null Given Info");
        if (fileInfo.getFile() == null)
            throwException("Null File");
        if (fileInfo.getFileFormat() == null || fileInfo.getFileFormat().isEmpty())
            throwException("Empty File Format");
    }

    private void throwException(String msg) throws IllegalFileInfoException {
        throw new IllegalFileInfoException(msg);
    }
}
