package progressoft.jip9.reader;

public enum TakenFile {
  SOURCE,TARGET;
  public static String getName(TakenFile takenFrom){
      switch (takenFrom){
        case SOURCE:
          return "SOURCE";
        case TARGET:
          return "TARGET";
      }
      return null;
  }

}
