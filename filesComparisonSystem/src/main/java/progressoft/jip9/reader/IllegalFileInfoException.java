package progressoft.jip9.reader;

import java.io.IOException;

public class IllegalFileInfoException extends IOException {
    public IllegalFileInfoException(String message) {
        super(message);
    }

    public IllegalFileInfoException(String message, Throwable cause) {
        super(message, cause);
    }
}
