package progressoft.jip9.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CSVReader implements Reader {

    private List<String> firstLine;

    @Override
    public List<FileData> read(File file) throws IllegalFileInfoException {
        List<FileData> dataList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            firstLine = Arrays.asList(reader.readLine().split(","));
            String line;
            while ((line = reader.readLine())  != null) {
                String[] split = line.split(",");
                dataList.add(getBuild(split));
            }

        } catch (IOException e) {
            throw new IllegalFileInfoException("problem happened when reading the file \n",e);
        }
        return dataList;
    }

    private FileData getBuild(String[] text) throws IllegalFileInfoException {
        return new FileData.DataBuilder()
                .setTransactionId(getFromIndex(text, "trans unique id"))
                .setValueDate(getFromIndex(text,"value date"))
                .setAmount(getFromIndex(text,"amount"))
                .setCurrencyCode(getFromIndex(text,"currecny"))
                .build();
    }

    public String getFromIndex(String[] text, String reference) throws IllegalFileInfoException {
        int index = firstLine.indexOf(reference);
        if(index <0)
           throw new IllegalFileInfoException("couldn't find reference of "+reference);
        return text[index];
    }
}
