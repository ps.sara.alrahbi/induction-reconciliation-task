package progressoft.jip9.reader;

public class Data implements Comparable<Data>{
    private final FileData fileData;
    private String takenFrom;

    public Data(FileData fileData, TakenFile takenFile) {
        this.takenFrom = TakenFile.getName(takenFile);
        this.fileData = fileData;
    }

    public FileData getFileData() {
        return fileData;
    }

    public String getTakenFrom() {
        return takenFrom;
    }


    @Override
    public int compareTo(Data data) {
        String sThis = getFileData().getTransactionId().substring(3);

        long intThis = Long.parseLong(sThis);
        long intThat = Long.parseLong(data.getFileData().getTransactionId().substring(3));
        if (intThis==intThat)
            return 0;
        if (intThis > intThat)
            return 1;
        return -1;
    }
}