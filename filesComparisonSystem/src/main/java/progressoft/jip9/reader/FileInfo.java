package progressoft.jip9.reader;


import java.io.File;
import java.util.List;

public class FileInfo {
    private String fileFormat;
    private File file;

    List<FileData> fileDataList;
    public List<FileData> getFileDataList() {
        return fileDataList;
    }


    public void setFileDataList(List<FileData> fileDataList) {
        this.fileDataList = fileDataList;
    }
    public String getFileFormat() {
        return fileFormat;
    }

    public File getFile() {
        return file;
    }


    public static class FileBuilder {
        FileInfo fileInfo = new FileInfo();

        public FileBuilder setFileFormat(String fileFormat) {
            fileInfo.fileFormat = fileFormat;
            return this;
        }

        public FileBuilder setFile(File file) {

            fileInfo.file = file;
            return this;
        }

        public FileInfo build() {
            return fileInfo;
        }
    }


}
