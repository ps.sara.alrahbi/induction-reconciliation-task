package progressoft.jip9.reader;

import java.math.BigDecimal;
import java.util.Currency;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FileData implements Comparable<FileData> {
    private String transactionId;
    private String valueDate;
    private BigDecimal amount;
    private Currency currencyCode;

    public String getTransactionId() {
        return transactionId;
    }

    public String getValueDate() {
        return valueDate;
    }


    public BigDecimal getAmount() {
        return this.amount.setScale(currencyCode.getDefaultFractionDigits());
    }

    public Currency getCurrencyCode() {
        return this.currencyCode;
    }

    @Override
    public int compareTo(FileData data) {

        String sThis = getTransactionId().substring(3);

        long intThis = Long.parseLong(sThis);
        long intThat = Long.parseLong(data.getTransactionId().substring(3));
        if (intThis==intThat)
            return 0;
        if (intThis > intThat)
            return 1;
        return -1;
    }



    public static class DataBuilder {
        FileData data = new FileData();
        private final HashMap<String, String> regexDateFormat = new HashMap<>();

        public DataBuilder setAmount(String amount) {
            data.amount = BigDecimal.valueOf(Double.parseDouble(amount));
            return this;
        }

        public DataBuilder setCurrencyCode(String currencyCode) throws IllegalFileInfoException {
            try {
                data.currencyCode = Currency.getInstance(currencyCode);
            }catch (IllegalArgumentException e){
                throw new IllegalFileInfoException("invalid Currency code \n",e);
            }
            return this;
        }

        public DataBuilder setTransactionId(String transactionId) {
            data.transactionId = transactionId;
            return this;
        }

        public DataBuilder setValueDate(String valueDate) {
            putInRegexMap();
            SimpleDateFormat takenFormat = getSimpleDateFormat(valueDate);
            if (takenFormat == null)
                throw new IllegalParsingException("can't define " + valueDate +
                        " as a date");

            parseToNeededFormat(valueDate, takenFormat);

            return this;

        }

        private void parseToNeededFormat(String valueDate, SimpleDateFormat givenFormat) {
            Date date;
            SimpleDateFormat wantedFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                date = givenFormat.parse(valueDate);
            } catch (ParseException e) {
                throw new IllegalParsingException("failed to parse \n ", e);
            }
            data.valueDate = wantedFormat.format(date);
        }

        public FileData build() {
            return data;
        }

        private SimpleDateFormat getSimpleDateFormat(String valueDate) {

            for (Map.Entry<String, String> entry : regexDateFormat.entrySet()) {
                String k = entry.getKey();
                String v = entry.getValue();
                if (valueDate.matches(k)) {
                    return new SimpleDateFormat(v);
                }
            }
            return null;
        }

        private void putInRegexMap() {
            regexDateFormat.put("[0-9]{4}-[0-1][0-9]-[0-3][0-9]", "yyyy-MM-dd");
            regexDateFormat.put("[0-3][0-9]-[0-1][0-9]-[0-9]{4}", "dd-MM-yyyy");
            regexDateFormat.put("[0-3][0-9]/[0-1][0-9]/[0-9]{4}", "dd/MM/yyyy");
            regexDateFormat.put("[0-9]{4}/[0-1][0-9]/[0-3][0-9]", "yyyy/MM/dd");
        }

    }


}
