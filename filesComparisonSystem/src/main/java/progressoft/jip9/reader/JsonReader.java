package progressoft.jip9.reader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class JsonReader implements Reader {


    @Override
    public List<FileData> read(File file) throws IllegalFileInfoException {
        List<FileData> fileList = new ArrayList<>();

        try {
            String content = Files.readString(file.toPath());
            JSONArray jsonArray = new JSONArray(content);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jObj = jsonArray.getJSONObject(i);

                fileList.add(new FileData.DataBuilder()
                        .setTransactionId(jObj.getString("reference"))
                        .setValueDate(jObj.getString("date"))
                        .setAmount(jObj.getString("amount"))
                        .setCurrencyCode(jObj.getString("currencyCode"))
                        .build());
            }
        } catch (IOException| JSONException e) {
            throw new IllegalFileInfoException("problem with reading json file:\n " + e);
        }

        return fileList;
    }
}
