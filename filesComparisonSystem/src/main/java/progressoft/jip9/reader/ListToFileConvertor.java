package progressoft.jip9.reader;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class ListToFileConvertor {
    private Path folderPath;

    public Path convertOutputToFile(SavedOutput savedOutput) throws IllegalFileInfoException {
        makeDestinationFolder("Result");
        List<StringJoiner> mismatchedReady = convertDataListToCSVList(savedOutput.getMismatched());
        List<StringJoiner> missingReady = convertDataListToCSVList(savedOutput.getMissingList());
        List<StringJoiner> matchingReady = convertFileDataListToCSVList(savedOutput.getMatchedList());
        createCSVFile("matching-transactions", matchingReady);
        createCSVFile("mismatched-transactions",mismatchedReady);
        createCSVFile("missing-transactions",missingReady);
        return folderPath;


    }

    private File createCSVFile(String fileName, List<StringJoiner> list) throws IllegalFileInfoException {
        Path path = Paths.get(String.valueOf(folderPath), fileName + ".csv");
        File file = new File(path.toString());
        try {
            file.createNewFile();
        } catch (IOException e) {
            throw new IllegalFileInfoException("couldn't create file in" + file.getPath(), e);
        }
        try (PrintWriter writer = new PrintWriter(file)) {
            for (StringJoiner line : list) {
                writer.println(line);
                writer.flush();
            }

        } catch (IOException e) {
            throw new IllegalFileInfoException("an error happened while writing to" +
                    fileName + " file", e);
        }
        return file;

    }

    private List<StringJoiner> convertFileDataListToCSVList(List<FileData> matchedList) {
        List<StringJoiner> builderList = new ArrayList<>();
        builderList.add(addMatchedHeader());
        matchedList.forEach(data -> {
            StringJoiner builder = getStringJoinerFromFileData(data);
            builderList.add(builder);
        });
        return builderList;

    }

    private StringJoiner addMatchedHeader() {
        return new StringJoiner(",")
                .add("transaction id")
                .add("amount")
                .add("currecny code")
                .add("value date");
    }

    private StringJoiner getStringJoinerFromFileData(FileData fileData) {
        StringJoiner builder = new StringJoiner(",");
        builder.add(fileData.getTransactionId());
        builder.add(fileData.getAmount().toPlainString());
        builder.add(fileData.getCurrencyCode().toString());
        builder.add(fileData.getValueDate());
        return builder;

    }


    public void makeDestinationFolder(String fileName) {
        Path dir = Paths.get("..", fileName);
        File file = new File(dir.toString());
        if (!file.exists())
            file.mkdir();
        folderPath = Path.of(file.getAbsolutePath());
    }

    public List<StringJoiner> convertDataListToCSVList(List<Data> mismatched) {
        List<StringJoiner> builderList = new ArrayList<>();
        builderList.add(addMismatchedHeader());
        mismatched.forEach(data -> {
            StringJoiner builder = getStringJoinerFromData(data);
            builderList.add(builder);
        });
        return builderList;

    }

    private StringJoiner getStringJoinerFromData(Data data) {
        StringJoiner builder = new StringJoiner(",");
        builder.add(data.getTakenFrom());
        FileData fileData = data.getFileData();
        builder.add(fileData.getTransactionId());
        builder.add(fileData.getAmount().toPlainString());
        builder.add(fileData.getCurrencyCode().toString());
        builder.add(fileData.getValueDate());
        return builder;
    }

    private StringJoiner addMismatchedHeader() {
        return new StringJoiner(",")
                .add("found in file")
                .add("transaction id")
                .add("amount")
                .add("currecny code")
                .add("value date");
    }
}
