package progressoft.jip9.reader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class SavedOutput {
    private List<Data> missingList=new ArrayList<>();
    private List<Data> MismatchedList =new ArrayList<>();
    private List<FileData> matchedList=new ArrayList<>();

    public List<Data> getMismatched() {
        return this.MismatchedList;
    }

    public List<Data> getMissingList() {
        return missingList;
    }

    public List<FileData> getMatchedList() {
        return matchedList;
    }

    public void addToMismatched(Data sourceData, Data targetData) {
        MismatchedList.add(sourceData);
        MismatchedList.add(targetData);
    }


    public void addToMatched(FileData fileData) {
        matchedList.add(fileData);
    }

    public void addToMissing(HashMap<String, Data> potentialMissingList) {
        missingList.addAll(potentialMissingList.values());
        Collections.sort(missingList);
    }
}
