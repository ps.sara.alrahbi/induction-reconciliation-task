package progressoft.jip9.reader;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileTest {

    public FileInfo getCSvFileInfo() {

        Path p = Paths.get("src", "main", "resources","sample-files", "input-files", "bank-transactions.csv");
        return new FileInfo.FileBuilder()
                .setFile(new File(String.valueOf(p)))
                .setFileFormat("csv")
                .build();
    }

    public FileInfo getInvalidJsonFileInfo() {

        Path p = Paths.get("src", "main", "resources","sample-files", "input-files", "online-banking-transactions.json");

        return new FileInfo.FileBuilder()
                .setFile(new File(String.valueOf(p)))
                .setFileFormat("csv")
                .build();
    }

    public FileInfo getJsonFileInfo() {
        Path p = Paths.get("src", "main", "resources","sample-files", "input-files", "online-banking-transactions.json");

        return new FileInfo.FileBuilder()
                .setFile(new File(String.valueOf(p)))
                .setFileFormat("json")
                .build();
    }
}
