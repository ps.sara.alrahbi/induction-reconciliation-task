package progressoft.jip9.reader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

public class OutputFilesTest extends FileTest {
    FileInfo source;
    FileInfo target;
    List<FileData> sourceList;
    List<FileData> targetList;
    CSVReader reader = new CSVReader();
    JsonReader jsonReader = new JsonReader();

    @BeforeEach
    void init() throws IllegalFileInfoException {
        source = getCSvFileInfo();
        target = getJsonFileInfo();
        sourceList = reader.read(source.getFile());
        targetList = jsonReader.read(target.getFile());
        source.setFileDataList(sourceList);
        target.setFileDataList(targetList);

    }

    @Test
    void givenCSVFile_whenToOutput_thenFileDir() {
        FileInfo info = getCSvFileInfo();

//        Assertions.assertEquals(info);

    }

    @Test
    void given_when_then() {

    }

    @Test
    void given2Files_whenToProcessOutput_then3OutputFiles() throws IllegalFileInfoException {

        ListsComparison compare = new ListsComparison(source, target);
        SavedOutput savedOutput = compare.compareTheLists();
        ListToFileConvertor convertor = new ListToFileConvertor();
        convertor.convertDataListToCSVList(savedOutput.getMismatched());
        Path result = convertor.convertOutputToFile(savedOutput);
        Assertions.assertNotNull(result);
        File file = result.toFile();
        Assertions.assertTrue(file.isDirectory());
        File[] contents = file.listFiles();
        Assertions.assertNotNull(contents);
        Assertions.assertEquals(3,contents.length);

    }
}
