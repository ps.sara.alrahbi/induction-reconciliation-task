package progressoft.jip9.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OutputCSVReader extends CSVReader {
    public List<Data> readIt(File file) throws IllegalFileInfoException {
        List<Data> dataList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine())  != null) {
                String[] split = line.split(",");
                dataList.add(new Data(getBuild(split),TakenFile.SOURCE));
            }

        } catch (IOException e) {
            throw new IllegalFileInfoException("problem happened when reading the file \n",e);
        }
        return dataList;
    }
    private FileData getBuild(String[] text) throws IllegalFileInfoException {
        return new FileData.DataBuilder()
                .setTransactionId(getFromIndex(text, "transaction id"))
                .setValueDate(getFromIndex(text, "value date"))
                .setAmount(getFromIndex(text, "amount"))
                .setCurrencyCode(getFromIndex(text, "currecny code"))
                .build();



    }

    public List<Data> readMissing(File toFile) throws IllegalFileInfoException {
        List<Data> dataList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(toFile))) {
            String line;
            while ((line = reader.readLine())  != null) {
                String[] split = line.split(",");
                dataList.add(new Data(getBuildMissingList(split),TakenFile.SOURCE));
            }

        } catch (IOException e) {
            throw new IllegalFileInfoException("problem happened when reading the file \n",e);
        }
        return dataList;
    }

    private FileData getBuildMissingList(String[] text) throws IllegalFileInfoException {
        return new FileData.DataBuilder()
                .setTransactionId(getFromIndex(text, "transaction id"))
                .setValueDate(getFromIndex(text, "value date"))
                .setAmount(getFromIndex(text, "amount"))
                .setCurrencyCode(getFromIndex(text, "currency code"))
                .build();
    }
}
