package progressoft.jip9.reader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileChecksTest extends FileTest {

    @Test
    void givenDifferentFormatFromFile_whenToCheck_thenException() {
        FileCheck fileCheck = new FileCheck();
        FileInfo fileInfo = getInvalidJsonFileInfo();
        Assertions.assertNotNull(fileInfo);
        IllegalFileInfoException actual = Assertions.assertThrows(IllegalFileInfoException.class,
                () -> fileCheck.checkFile(fileInfo));
        String expected = "provided different format from file type: " +
                "file type: application/json" +
                " format given: " + fileInfo.getFileFormat();
        Assertions.assertEquals(expected, actual.getMessage());
        Path p = Paths.get("src", "main", "resources", "input-files");
        FileInfo info = new FileInfo.FileBuilder()
                .setFile(new File(String.valueOf(p)))
                .setFileFormat("excel")
                .build();

        IllegalFileInfoException actual2 = Assertions.assertThrows(IllegalFileInfoException.class,
                () -> fileCheck.checkFile(info));
        Assertions.assertEquals("File path should be given", actual2.getMessage());

    }


    @Test
    void givenNullInfo_whenToCheck_thenException() {
        FileCheck fileCheck = new FileCheck();
        IllegalFileInfoException nullException = Assertions.assertThrows(IllegalFileInfoException.class,
                () -> fileCheck.checkFile(null));
        Assertions.assertEquals("Null Given Info", nullException.getMessage());
        //null file
        FileInfo info = new FileInfo.FileBuilder().
                setFile(null)
                .setFileFormat("jason")
                .build();
        IllegalFileInfoException nullFile = Assertions.assertThrows(IllegalFileInfoException.class,
                () -> fileCheck.checkFile(info));
        Assertions.assertEquals("Null File", nullFile.getMessage());
        //null format
        Path p = Paths.get("src", "main", "resources", "input-files", "bank-transactions.csv");
        FileInfo infoEmptyFormat = new FileInfo.FileBuilder()
                .setFile(new File(String.valueOf(p)))
                .setFileFormat(null)
                .build();
        IllegalFileInfoException nullFormat = Assertions.assertThrows(IllegalFileInfoException.class,
                () -> fileCheck.checkFile(infoEmptyFormat));
        Assertions.assertEquals("Empty File Format", nullFormat.getMessage());

    }


    @Test
    void givenDifferentDateFormats_whenToStoreData_thenOutputToYYYY_MM_dd() {
        FileData givenData = new FileData.DataBuilder()
                .setValueDate("2020-01-20")
                .build();
        FileData givenData2 = new FileData.DataBuilder()
                .setValueDate("2020/01/20")
                .build();
        FileData givenData3 = new FileData.DataBuilder()
                .setValueDate("20/01/2020")
                .build();
        FileData givenData4 = new FileData.DataBuilder()
                .setValueDate("20-01-2020")
                .build();
        String expectedOutPut = "2020-01-20";
        Assertions.assertEquals(expectedOutPut, givenData.getValueDate(), "different output than wanted");
        Assertions.assertEquals(expectedOutPut, givenData2.getValueDate(), "different output than wanted");
        Assertions.assertEquals(expectedOutPut, givenData3.getValueDate(), "different output than wanted");
        Assertions.assertEquals(expectedOutPut, givenData4.getValueDate(), "different output than wanted");
    }

    @Test
    void givenInvalidDateFormats_whenToStoreData_thenException() {
        try {
            new FileData.DataBuilder()
                    .setValueDate("2-01-2020")
                    .build();
            Assertions.fail("should have thrown exception");
        } catch (IllegalParsingException e) {
            Assertions.assertEquals("can't define 2-01-2020 as a date", e.getMessage());
        }
        try {
            new FileData.DataBuilder()
                    .setValueDate("20-01-20")
                    .build();
            Assertions.fail("should have thrown exception");
        } catch (IllegalParsingException e) {
            Assertions.assertEquals("can't define 20-01-20 as a date", e.getMessage());
        }
        try {
            new FileData.DataBuilder()
                    .setValueDate("20/01/20")
                    .build();
            Assertions.fail("should have thrown exception");
        } catch (IllegalParsingException e) {
            Assertions.assertEquals("can't define 20/01/20 as a date", e.getMessage());
        }

    }
    @Test
    void givenInvalidCurrency_whenToStoreData_thenException() {
        try {
             new FileData.DataBuilder()
                    .setTransactionId("TR-47884222201")
                    .setValueDate("2020-01-20")
                    .setAmount("140.00")
                    .setCurrencyCode("USsD")
                    .build();
            Assertions.fail("should throw an exception ");
        } catch (Exception e) {
            if (!(e instanceof IllegalFileInfoException))
                Assertions.fail("different exception was thrown");
            Assertions.assertEquals("invalid currency code", e.getMessage(), "different massage");
        }
    }

}
