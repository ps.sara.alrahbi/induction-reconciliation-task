package progressoft.jip9.reader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ComparingTest extends FileTest {
    FileInfo source;
    FileInfo target;
    List<FileData> sourceList;
    List<FileData> targetList;
    CSVReader reader = new CSVReader();
    JsonReader jsonReader = new JsonReader();
    List<Data> dataList;
    ListsComparison compare;
    SavedOutput savedOutput;

    @BeforeEach
    void init() throws IllegalFileInfoException {
        source = getCSvFileInfo();
        target = getJsonFileInfo();
        sourceList = reader.read(source.getFile());
        targetList = jsonReader.read(target.getFile());
        source.setFileDataList(sourceList);
        target.setFileDataList(targetList);

    }

    @Test
    void give2File_whenToSort_thenList() throws IllegalFileInfoException {

        ListsComparison compare = new ListsComparison(source, target);
        List<FileData> actual = compare.getTargetList();
        Path path = Paths.get("src", "test", "orderedFileToTest.json");
        List<FileData> expected = jsonReader.read(new File(String.valueOf(path)));

        Assertions.assertEquals(expected.size(), actual.size(), "should be same length");

        for (int i = 0, expectedSize = expected.size(); i < expectedSize; i++) {
            FileData expect = expected.get(i);
            FileData act = actual.get(i);
            if (!act.getTransactionId().equalsIgnoreCase(expect.getTransactionId()))
                Assertions.fail("file not ordered");
            if (!act.getValueDate().equals(expect.getValueDate()))
                Assertions.fail("file value dates are not ordered with the rest");
            if (!act.getAmount().equals(expect.getAmount()))
                Assertions.fail("file amounts are not ordered with the rest");
            if (!act.getCurrencyCode().equals(expect.getCurrencyCode()))
                Assertions.fail("file currency codes are not ordered with the rest");

        }
    }


    @Test
    void give2File_whenToCompareMismatched_thenMismatchedList() throws IllegalFileInfoException {
        Path p = Paths.get("src", "main", "resources", "sample-files", "result-files", "mismatched-transactions.csv");
        OutputCSVReader reader = new OutputCSVReader();
        given(p, reader);
        Assertions.assertNotNull(savedOutput, "output shouldn't be null");
        List<Data> mismatched = savedOutput.getMismatched();
        Assertions.assertNotNull(mismatched);
        Assertions.assertFalse(mismatched.isEmpty(), "mismatched list shouldn't be empty");
        Assertions.assertEquals(4, mismatched.size(), "output length of different size");
        for (int i = 0; i < mismatched.size(); i++) {
            if (dataList.get(i).getFileData().compareTo(mismatched.get(i).getFileData()) != 0)
                Assertions.fail("they are not same");
        }

    }

    private void given(Path p, OutputCSVReader oReader) throws IllegalFileInfoException {
        dataList = oReader.readIt(p.toFile());
        compare = new ListsComparison(source, target);
        savedOutput = compare.compareTheLists();
    }

    @Test
    void give2File_whenToCompareMatched_thenMatchedList() throws IllegalFileInfoException {
        Path p = Paths.get("src", "main", "resources", "sample-files", "result-files", "matching-transactions.csv");
        OutputCSVReader reader = new OutputCSVReader();
        given(p, reader);
        Assertions.assertNotNull(savedOutput, "output shouldn't be null");
        List<FileData> matched = savedOutput.getMatchedList();
        Assertions.assertFalse(matched.isEmpty(), "matched list shouldn't be empty");
        Assertions.assertEquals(3, matched.size(), "matched list not the same size");
        for (int i = 0; i < matched.size(); i++) {
            if (dataList.get(i).getFileData().compareTo(matched.get(i)) != 0)
                Assertions.fail("they are not same");
        }


    }

    @Test
    void give2File_whenToCompareMissing_thenTheMissingList() throws IllegalFileInfoException {
        Path p = Paths.get("src", "main", "resources", "sample-files", "result-files", "missing-transactions.csv");
        OutputCSVReader reader = new OutputCSVReader();
        dataList = reader.readMissing(p.toFile());
        compare = new ListsComparison(source, target);
        savedOutput = compare.compareTheLists();
        List<Data> missing = savedOutput.getMissingList();
        Assertions.assertFalse(missing.isEmpty(), "matched list shouldn't be empty");
        Assertions.assertEquals(dataList.size(), missing.size(), "matched list not the same size");
        for (int i = 0; i < missing.size(); i++) {
            FileData fileData = dataList.get(i).getFileData();
            FileData missingList = missing.get(i).getFileData();
            if (fileData.compareTo(missingList) != 0)
                Assertions.fail(fileData + " and " + missingList + " are not same");
        }

    }



}
