package progressoft.jip9.reader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

public class FileReaderTest extends FileTest {


    @Test
    void givenJson_whenToRead_thenFileDataList() throws IllegalFileInfoException {
        FileInfo file = getJsonFileInfo();
        JsonReader reader = new JsonReader();
        FileData expectedData = new FileData.DataBuilder()
                .setTransactionId("TR-47884222201")
                .setValueDate("2020-01-20")
                .setAmount("140.00")
                .setCurrencyCode("USD")
                .build();
        try {
            List<FileData> list = reader.read(file.getFile());
            Assertions.assertFalse(list.isEmpty(), "list shouldn't be empty");
            Assertions.assertEquals(7, list.size());
            Assertions.assertEquals(expectedData.getTransactionId(), list.get(0).getTransactionId(), "transaction id  is not same");
            Assertions.assertEquals(expectedData.getValueDate(), list.get(0).getValueDate(), "different value date than expected");
            Assertions.assertEquals(expectedData.getAmount(), list.get(0).getAmount(), "different amount from expected");
            Assertions.assertEquals(expectedData.getCurrencyCode(), list.get(0).getCurrencyCode(), "different currencyCode from expected");
        } catch (IllegalFileInfoException e) {
            Assertions.fail("a normal json file shouldn't throw exception");
        }
    }

    @Test
    void givenCSV_whenToRead_thenFileDataList() throws IllegalFileInfoException {
        FileInfo file = getCSvFileInfo();
        CSVReader reader = new CSVReader();
        FileData expectedData = new FileData.DataBuilder()
                .setTransactionId("TR-47884222201")
                .setValueDate("2020-01-20")
                .setAmount("140")
                .setCurrencyCode("USD")
                .build();
        String[] givenLine = {"TR-47884222201", "online transfer", "140", "USD", "donation", "2020-01-20", "D"};

        try {
            List<FileData> list = reader.read(file.getFile());
            Assertions.assertFalse(list.isEmpty(), "list shouldn't be empty");
            Assertions.assertEquals(expectedData.getTransactionId(), list.get(0).getTransactionId(), "transaction id  is not same");
            Assertions.assertEquals(expectedData.getValueDate(), list.get(0).getValueDate(), "different value date than expected");
            Assertions.assertEquals(expectedData.getAmount(), list.get(0).getAmount(), "different amount from expected");
            Assertions.assertEquals(expectedData.getCurrencyCode(), list.get(0).getCurrencyCode(), "different currencyCode from expected");
            //getFromIndex testing
            Assertions.assertEquals("USD", reader.getFromIndex(givenLine, "currecny"), "should have returned the same index");

        } catch (IllegalFileInfoException e) {
            Assertions.fail("a normal csv file shouldn't throw exception");
        }

        IllegalFileInfoException givenException = Assertions.assertThrows(IllegalFileInfoException.class,
                () -> reader.getFromIndex(givenLine, "cy"),
                "should have throw exception when it is not there");

        Assertions.assertEquals("couldn't find reference of cy", givenException.getMessage());

    }

    @Test
    void given2DifferentFormatInputs_whenUsingStrategyClass_thenGoToDesignedClass() throws IllegalFileInfoException {

        FileInfo source = getCSvFileInfo();
        FileInfo target = getJsonFileInfo();

        CSVReader csvReader = new CSVReader();
        JsonReader jsonReader = new JsonReader();

        StrategyReader strategy = new StrategyReader(csvReader);

        Assertions.assertNotNull(strategy.getReader(), "reader shouldn't be null");
        Assertions.assertEquals(csvReader, strategy.getReader(), "not the same as the ones assigned to");

        List<FileData> fileData = strategy.readStrategy(source).getFileDataList();
        Assertions.assertNotNull(fileData, "shouldn't be null");
        strategy.changeStrategy(jsonReader);
        Assertions.assertEquals(jsonReader, strategy.getReader(), "should be changed from the previous one");
        List<FileData> fileData2 = strategy.readStrategy(target).getFileDataList();
        Assertions.assertNotEquals(fileData, fileData2, "shouldn't output same thing");


    }

    @Test
    void givenNullFormat_whenToGetReader_thenException() {
        FileInfo source = getCSvFileInfo();
        FileProcessor processor = new FileProcessor();

        String fileFormat = source.getFileFormat();
        Reader readerClass = processor.getReaderClass(fileFormat);
        Assertions.assertNotNull(readerClass);
    }

    @Test
    void given2DifferentFormatInputs_whenToProcess_then3OutputFiles() throws IllegalFileInfoException {
        FileInfo source = getCSvFileInfo();
        FileInfo target = getJsonFileInfo();

        FileProcessor processor = new FileProcessor();
        Path path = processor.processFiles(source, target);

        Assertions.assertNotNull(path, "should not return empty path");
        Assertions.assertTrue(path.isAbsolute(), "should give absolute path");

        File file = path.toFile();
        Assertions.assertTrue(file.isDirectory(), "path should be directory");

        File[] contents = file.listFiles();
        Assertions.assertNotNull(contents, "file list shouldn't be null");
        Assertions.assertEquals(3, contents.length, "path should have contain three files");

    }

}
