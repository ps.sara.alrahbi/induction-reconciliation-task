package com.progressoft.jip9.servlets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

class ValidationServletTest extends Mockito {

    @Test
    void givenNullParameters_whenDoPost_thenException() throws IOException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        when(request.getParameter("username")).thenReturn("user");
        when(request.getParameter("password")).thenReturn("secret");

        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);
        try {
            new ValidationServlet().doPost(request, response);
        } catch (ServletException e) {
            Assertions.fail("shouldn't throw this type of exception");
        }
      /*  verify(request, atLeast(1)).getParameter("username");
        writer.flush(); // it may not have been flushed yet...
        Assertions.assertTrue(stringWriter.toString().contains("My expected string"));*/

    }
}