package com.progressoft.jip9.servlets;

import com.progressoft.jip9.DBHandler.CheckAccess;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ValidationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect(req.getContextPath() + "/login.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        if (username.isBlank() || password.isBlank())
            resp.sendError(400, "the input given was blank");
        CheckAccess checkAccess = new CheckAccess();
        if (checkAccess.validateLogin(username, password)) {
            HttpSession session = req.getSession(true);
            session.setAttribute("username", username);
            session.setMaxInactiveInterval(600);

            resp.sendRedirect(req.getContextPath() + "/upload");
        }
        String s = "username or password is invalid";
        req.setAttribute("invalidMassage", s);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/login.jsp");
        dispatcher.include(req, resp);


    }
}
