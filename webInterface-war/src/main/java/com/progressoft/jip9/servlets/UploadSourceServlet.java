package com.progressoft.jip9.servlets;

import progressoft.jip9.reader.FileInfo;
import progressoft.jip9.reader.FileProcessor;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


public class UploadSourceServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext()
                .getRequestDispatcher("/WEB-INF/views/uploadFile.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParts() == null)
            sendError(req, resp, "nothing was taken");
        Part sourceFile = req.getPart("sourceFile");
        String sContentType = sourceFile.getContentType();
        String sGivenType = req.getParameter("sourceType");
        String sName = req.getParameter("sourceName");
        Part targetFile = req.getPart("targetFile");
        String tContentType = targetFile.getContentType();
        String tGivenType = String.valueOf(req.getParameter("targetType"));
        String tName = req.getParameter("targetName");

        if (!((checkContent(sContentType, sGivenType) && checkContent(tContentType, tGivenType)))) {
            sendError(req, resp, "file should be same as chosen type");
            return;
        }
        if (!(checkFileName(sName) && checkFileName(tName)))
            sendError(req, resp, "file names should not be null or have . or / ");
        String pathOfFolder = String.valueOf(Paths.get("..", "uploaded"));
        File sTempFile = uploadFile(sourceFile, pathOfFolder, sGivenType, sName);
        FileInfo sInfo = getFileInfoBuild(sGivenType, sTempFile);
        File tTempFile = uploadFile(targetFile, pathOfFolder, tGivenType, tName);
        FileInfo tInfo = getFileInfoBuild(tGivenType, tTempFile);
        FileProcessor processor = new FileProcessor();
        Path resultPath = processor.processFiles(sInfo, tInfo);
        req.setAttribute("resultPath", resultPath);
        req.getRequestDispatcher("resultServlet").forward(req, resp);

        //  req.getRequestDispatcher("/WEB-INF/views/resultPage.jsp").forward(req, resp);


    }

    private FileInfo getFileInfoBuild(String GivenType, File TempFile) {
        return new FileInfo.FileBuilder().setFile(TempFile).setFileFormat(GivenType).build();
    }

    private boolean checkFileName(String name) {
        boolean isOk = true;
        if (name.isBlank() || name.matches(",|/|.+"))
            isOk = false;
        return isOk;
    }


    private File uploadFile(Part filePart, String pathOfFolder, String givenType, String fileName) throws IOException {
        filePart.write(fileName);
        InputStream sourceIn = filePart.getInputStream();
        Path sourceFilePath = Paths.get(pathOfFolder, fileName + "." + givenType);
        File tempFile = new File(sourceFilePath.toString());
        if (!tempFile.exists())
            if (!tempFile.mkdir())
                throw new IOException("could not make the wanted directory");

        java.nio.file.Files.copy(sourceIn, tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

        sourceIn.close();
        return tempFile;
    }

    private void sendError(HttpServletRequest req, HttpServletResponse resp, String msg) throws ServletException, IOException {
        req.setAttribute("error", msg);
        req.getRequestDispatcher("WEB-INF/views//uploadFile.jsp").forward(req, resp);
    }

    private boolean checkContent(String sContentType, String fileType) {

        if (!(sContentType.contains(fileType))) {
            return fileType.contains("csv") && ContentUseWindows(sContentType);
        }
        return true;
    }

    private boolean ContentUseWindows(String contentType) {
        return contentType.contains("vnd") && contentType.contains("excel");
    }
}
