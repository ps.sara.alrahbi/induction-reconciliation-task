package com.progressoft.jip9.DBHandler;

import org.apache.commons.codec.binary.Base64;

import java.sql.*;

public class CheckAccess {
    public boolean validateLogin(String username, String password) {
        boolean isOk = false;
        String sql = "SELECT * FROM userInfo where username=? and password=?";

        try (Connection con = getConnection()) {

            try (PreparedStatement ps = con.prepareStatement(sql)) {
                ps.setString(1, username);
                ps.setString(2, encode(password));

                try (ResultSet resultSet = ps.executeQuery()) {
                    if (resultSet.next())
                        isOk = true;
                }
            }

        } catch (SQLException e) {
            isOk = false;
        }
        return isOk;
    }

    Connection getConnection() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/comparedb";
        String user = "root";
        String pass = "saraRoot";

        Connection connection = DriverManager.getConnection(url, user, pass);
        return connection;

    }

    private String encode(String pas) {
        byte[] bytesEncoded = Base64.encodeBase64(pas.getBytes());
        return new String(bytesEncoded);
    }

}
