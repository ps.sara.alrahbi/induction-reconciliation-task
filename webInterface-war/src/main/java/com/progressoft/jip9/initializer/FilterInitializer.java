package com.progressoft.jip9.initializer;

import com.progressoft.jip9.filter.UserFilter;

import javax.servlet.*;
import java.util.EnumSet;
import java.util.Set;

public class FilterInitializer implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> set, ServletContext ctx) throws ServletException {
//        registerUserFilter(ctx);
    }

    private void registerUserFilter(ServletContext ctx) {
        FilterRegistration.Dynamic domainCheckFilter = ctx.addFilter("domainCheckFilter", new UserFilter());
        domainCheckFilter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
    }
}
