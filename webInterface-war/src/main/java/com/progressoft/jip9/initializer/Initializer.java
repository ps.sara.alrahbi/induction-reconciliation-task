package com.progressoft.jip9.initializer;

import com.progressoft.jip9.servlets.ResultServlet;
import com.progressoft.jip9.servlets.UploadSourceServlet;
import com.progressoft.jip9.servlets.ValidationServlet;

import javax.servlet.*;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Set;

public class Initializer implements ServletContainerInitializer {

    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        initializeValidationServlet(servletContext);
        initializeUploadServlet(servletContext);
        initializeResultsServlet(servletContext);

    }

    private void initializeResultsServlet(ServletContext servletContext) {
        ResultServlet resultServlet=new ResultServlet();
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("resultServlet", resultServlet);
        servletRegistration.addMapping("/result");
    }

    private void initializeUploadServlet(ServletContext cs) {
        UploadSourceServlet sourceServlet = new UploadSourceServlet();
        ServletRegistration.Dynamic servletRegistration = cs.addServlet("sourceServlet", sourceServlet);
        servletRegistration.setMultipartConfig(

                new MultipartConfigElement("../uploaded", 1048576, 1048576, 262144));
        servletRegistration.addMapping("/upload");
        try {
            String location = String.valueOf(Files.createTempDirectory(null));
            MultipartConfigElement multipartConfig = new MultipartConfigElement(location);
            servletRegistration.setMultipartConfig(multipartConfig);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void initializeValidationServlet(ServletContext servletContext) {
        ValidationServlet validationServlet = new ValidationServlet();
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("ValidationServlet", validationServlet);
        servletRegistration.addMapping("/validate");
    }
}
