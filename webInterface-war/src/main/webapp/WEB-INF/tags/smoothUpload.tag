<script type="text/javascript">
    const myForm = document.getElementById("myForm");
    const targetTable = document.getElementById("target");
    const sourceTable = document.getElementById("source");

    function cancelIt() {
        myForm.reset();
        goToSource();
    }

    function goToTarget() {
        sourceTable.style.display = "none";
        targetTable.style.display = "block";
        document.getElementById("confirm").style.display = "none";
        changeTitle("Target Upload")
    }

    function goToSource() {
        sourceTable.style.display = "block";
        targetTable.style.display = "none";
        document.getElementById("confirm").style.display = "none";
        changeTitle("source upload");
    }

    function changeTitle(name) {
        document.title = name;
    }

    function fillInTablesInfo() {
        let sourceName = document.getElementById("sourceName").value;
        let sourceType = document.getElementById("sourceType").value;
        let targetName = document.getElementById("targetName").value;
        let targetType = document.getElementById("targetType").value;
        document.getElementById("outputSourceName").innerHTML = sourceName;
        document.getElementById("outputSourceType").innerHTML = sourceType;
        document.getElementById("outputTargetName").innerHTML = targetName;
        document.getElementById("outputTargetType").innerHTML = targetType;
    }

    function afterTarget() {
        for (let i = 3; i < (myForm.elements.length); i++) {
            if (myForm.elements[i].value === '' && myForm.elements[i].hasAttribute('required')) {
                alert("please fill all fields");
                return false;
            }
        }

        targetTable.style.display = "none";
        document.getElementById("confirm").style.display = "block";
        fillInTablesInfo();
        changeTitle("Ready To Compare")

    }

    function gitIt() {
        for (let i = 0; i < 3; i++) {
            if (myForm.elements[i].value === '' && myForm.elements[i].hasAttribute('required')) {
                alert("please fill all fields");
                return false;
            }
        }
        goToTarget();

    }

</script>