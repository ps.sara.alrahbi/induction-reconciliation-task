<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Source Upload </title>
</head>
<body>

<tags:logoutButton/>
<h3>${error}</h3>
<form method="post" enctype="multipart/form-data" id="myForm">
    <table id="source">
        <tr>
            <td>Source Name:</td>
            <td><input type="text" id="sourceName" name="sourceName" required/></td>
        </tr>
        <tr>
            <td>File Location:</td>
            <td><input type="file" name="sourceFile" required/></td>
        </tr>
        <tr>
            <td>File Type:</td>
            <td>
                <select name="sourceType" id="sourceType">
                    <option>csv</option>
                    <option>json</option>
                </select>
            </td>
        </tr>
        <tr>
<%--            <td><input type="button" value="next" onclick="gitIt()"/></td>--%>
            <td><input type="submit"></td>
        </tr>

    </table>
</form>
<%--<form>
    <table id="target" style="display: none;">
        <tr>
            <td>target Name:</td>
            <td><input name="targetName" id="targetName" type="text" required/></td>
        </tr>
        <tr>
            <td>File Location:</td>
            <td><input type="file" name="targetFile" required/></td>
        </tr>
        <tr>
            <td>File Type:</td>
            <td>
                <select name="targetType" id="targetType">
                    <option>csv</option>
                    <option>json</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><input type="button" value="back" onclick="goToSource()"/></td>
            <td><input type="button" value="next" onclick="afterTarget()"/></td>

        </tr>
    </table>
    <table id="confirm" style="display: none;">
        <tr>
            <td>
                <table border="1px">
                    <tr>
                        <td colspan="2"> source</td>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td id="outputSourceName"></td>
                    </tr>
                    <tr>
                        <td>type:</td>
                        <td id="outputSourceType"></td>
                    </tr>
                </table>
            </td>
            <td>
                <table border="1px">
                    <tr>
                        <td colspan="2">target</td>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td id="outputTargetName"></td>
                    </tr>
                    <tr>
                        <td>type:</td>
                        <td id="outputTargetType"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td>result options:</td>
            <td>
            <select name="downloadType" id="downloadType">
                <option>csv</option>
                <option>json</option>
            </select></td>
        </tr>

        <tr>
            <td><input type="button" value="cancel" onclick="cancelIt()"/></td>
            <td><input type="button" value="back" onclick="goToTarget()"/>
            <input type="submit" value="compare"/></td>
        </tr>
    </table>
</form>

<tags:smoothUpload/>--%>
</body>
</html>
